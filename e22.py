#!/usr/bin/env python3

with open("e22names.txt") as rfp:
    content = rfp.read()

names = []
for name in content.split(','):
    names.append(name.strip('"'))

total = 0
for i, name in enumerate(sorted(names)):
    total += (i+1) * sum((ord(ch) - ord("A") + 1) for ch in name)

print(total)