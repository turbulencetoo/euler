#!/usr/bin/env python3
import JLib

triangle = 1
incr = 2

while 1:
    num = JLib.count_divisors(triangle)
    if num > 500:
        break
    triangle+=incr
    incr += 1

print(triangle)