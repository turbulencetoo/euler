#!/usr/bin/env python3

import math

def sieve(n):
    primes = [[i, True] for i in range(n)]
    for i in range(2, int(math.sqrt(n)+1)):
        for p in range(2*i,n,i):
            primes[p][1] = False

    return [i[0] for i in primes if i[1]]


def main():
    primes = sieve(2000000)
    print(primes[:20])
    print(sum(primes))

if __name__ == '__main__':
     main() 

