#!/usr/bin/env python3
from e18 import Node, Tree, get_best_path

def main():
    tree_list = []
    with open("e67tree.txt") as rfp:
        for line in rfp:
            row = []
            line = line.strip()
            line = line.split()
            if not line:
                continue
            for num in line:
                row.append(int(num))
            tree_list.append(row)
    tree = Tree(tree_list)
    print(get_best_path(tree))


if __name__ == '__main__':
    main()