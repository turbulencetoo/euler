#!/usr/bin/env python3

heredoc= """75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
"""

class Node:
    def __init__(self, value):
        self.parents = []
        self.kids = []
        self.value = value
        self.best_sum = value

class Tree:
    def __init__(self, tree_list):
        '''
        Takes a list lists of ints that represnt the tree, and builds the tree structure
        '''
        self.rows = []
        self.root = None

        for i, row in enumerate(tree_list):
            if i == 0:
                self.root = Node(row[0])
                self.rows.append([self.root])
                continue
            this_row = []
            for j, value in enumerate(row):
                new_node = Node(value)
                if j == 0:
                    parents = (self.rows[i-1][0],)  # First item from above row
                elif j == len(row) - 1:
                    parents = (self.rows[i-1][-1],)  # Last item from above row
                else:
                    parents = (self.rows[i-1][j-1], self.rows[i-1][j])  # Two parent case

                for parent in parents:
                    new_node.parents.append(parent)
                    parent.kids.append(new_node)
                this_row.append(new_node)
            self.rows.append(this_row)

    def __repr__(self):
        num_pad = 2 * len(self.rows)
        total_str = []
        for row in self.rows:
            row_str = []
            row_str.append(" "*num_pad)
            for node in row:
                row_str.append("{:02d}".format(node.value))
                row_str.append("  ")
            num_pad -= 2
            row_str.append(" "*num_pad)
            row_str.append("\n")
            total_str.append(''.join(row_str))
        return ''.join(total_str)

    def __str__(self):
        return repr(self)


def get_best_path(tree):
    for row in reversed(tree.rows):
        for node in row:
            if not node.kids:
                continue
            node.best_sum += max(n.best_sum for n in node.kids)
    return tree.root.best_sum

def main():
    tree_list = []
    for line in heredoc.split("\n"):
        line = line.strip()
        if not line:
            continue
        row = []
        for num in line.split():
            row.append(int(num))
        tree_list.append(row)
    print(tree_list)
    tree = Tree(tree_list)
    print(tree)

    print(get_best_path(tree))

if __name__ == '__main__':
    main()
