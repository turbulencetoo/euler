#!/usr/bin/env python3
import itertools
pdie = [1,2,3,4]
cdie = [1,2,3,4,5,6]

poptions = []
coptions = []

poptions = itertools.product(pdie, repeat=9)
coptions = itertools.product(cdie, repeat=6)

ptotals = (sum(pop) for pop in poptions)
ctotals = (sum(cop) for cop in coptions)

fights = itertools.product(ptotals, ctotals)


total = 0
pete_wins = 0
for f in fights:
    total += 1
    if total %1000000 ==0:
        print(total)
    if f[0] > f[1]:
        pete_wins += 1

print(total, pete_wins)