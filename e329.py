#!/usr/bin/env python3
from fractions import Fraction as F
from JLib import gen_primes

def move_frog(cur_tier):
    '''
    Given a list of probabilities, performs the frog movement.
    [1/2, 1/3, 1/4] -> [1/6, 3/4, 1/6]
    both outsides move inside, the middle squares go equally to the outsides.
    '''
    new_tier = [F(0, 1) for i in cur_tier]
    
    # Handle the edge cases
    new_tier[1] += cur_tier[0]
    new_tier[-2] += cur_tier[-1]

    # divvy up cur_tier probability between its neighbors
    for i in range(1, len(cur_tier)-1):
        new_tier[i-1] += F(1, 2) * cur_tier[i]
        new_tier[i+1] += F(1, 2) * cur_tier[i]

    return new_tier


def modify_probability(cur_tier, primes, croak):
    '''
    Given a list of probabilities, and the next croak in the sequence, returns the list
    of probabilities that the frog does emit the next croak
    '''
    return [cur_tier[i] * (F(2, 3) if primes[i+1] == croak else F(1,3)) for i in range(len(cur_tier))]


def main():
    croaks = [c == 'P' for c in "PPPPNNPPPNPPNPN"]
    primes = gen_primes(501)

    cur_tier = [F(1, 500) for i in range(500)]

    for i in range(len(croaks)):
        prob_mod_tier = modify_probability(cur_tier, primes, croaks[i])
        cur_tier = move_frog(prob_mod_tier)  # technically dont need to do this after croak 15 but it doesnt change sum of probabilities

    print(sum(cur_tier))

if __name__ == '__main__':
    main()