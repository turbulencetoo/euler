#!/usr/bin/env python3

SIZE = 21

def fill_diagonal(cur_row, cur_col):
    while (0 <= cur_row < SIZE) and (0 <= cur_col < SIZE):
        print(cur_row, cur_col)
        if cur_row+1 < SIZE:
            grid[cur_row][cur_col] += grid[cur_row+1][cur_col]
        if cur_col+1 < SIZE:
            grid[cur_row][cur_col] += grid[cur_row][cur_col+1]
        cur_row-=1
        cur_col+=1
    print("end diag -----------------------------")

grid = [[0 for i in range(SIZE)] for j in range(SIZE)]
grid[SIZE-1][SIZE-1] = 1

for i in range(SIZE-2, -1, -1):
    cur_row, cur_col = SIZE-1, i
    fill_diagonal(cur_row, cur_col)
for i in range(SIZE-2, -1, -1):
    cur_row, cur_col = i, 0
    fill_diagonal(cur_row, cur_col)

for row in grid:
    for item in row:
        print("{:<12}".format(item), end=' ')
    print()
print(grid[0][0])