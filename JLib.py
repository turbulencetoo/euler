import math

def gen_primes(n):
    '''
    find all primes less than n
    sieve that returns a 0-indexed list [0, 0, 1, 1, 0, 1, 0 ...]
    that represents which squares are prime  0  1  2  3  4  5  6
    '''
    l = [1 for i in range(n)]
    l[0] = None  # 0 doesnt belong 
    l[1] = 0     # 1 is not prime
    i = 2
    for i in range(2, math.ceil(math.sqrt(n))+1):
        if not l[i]:
            # don't worry about non-primes, they've been covered by their divisors
            continue
        for j in range(i**2, n, i):
            l[j] = 0
    return l

def pfac(n):
    '''
    find the prime factorization of n, returned as a dict, mapping factors to multiplicities
    pfac(84) -> {2: 2, 3: 1, 7: 1]
    '''
    fac = {}
    num = n
    div = 2
    while div ** 2 <= num:
        while num % div == 0:
            fac[div] = fac.get(div, 0) + 1
            num //= div
        div += 1
    if num > 1:
        # 'num' contains the last prime divisor not added in the loop
        fac[num] = fac.get(num, 0) + 1

    return fac

def count_divisors(n):
    '''
    use the fact that number of total divisors = i_x+1 * i_y+1 ... for all i multiplicites of each factors
    so 84 = 2*2*3*7, number of divisors is( 2+1)(1+1)(1+1) = 3*2*2 = 12
                                           22    3    7
    '''
    nums = pfac(n).values()
    num_divisors = 1
    for num in nums:
        num_divisors *= num + 1
    return num_divisors

def main():
    '''testing'''
    for i, val in enumerate(gen_primes(501)):
        print(i, val)
    
    print(pfac(600851475143))
    print(count_divisors(600851475143))
if __name__ == '__main__':
    main()
