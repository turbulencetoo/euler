#!/usr/bin/env python3
import numpy
'''
1  2  3  4  5
6  7  8  9  10
11 12 13 14 15
16 17 18 19 20
21 22 23 24 25
'''

adj = { 1: set([2,6]), 2: set([1,3,7]), 3: set([2,8,4]), 4: set([3,9,5]), 5: set([4,10]), 6: set([1,7,11]), 7: set([2,6,8,12]), 8: set([3,7,9,13]), 9: set([4,8,10,14]), 10: set([5,9,15]), 11: set([6,12,16]), 12: set([7,11,13,17]), 13: set([8,12,14,18]), 14: set([9,13,15,19]), 15: set([10,14,20]), 16: set([11,17,21]), 17: set([12,16,18,22]), 18: set([13,17,19,23]), 19: set([14,18,20,24]), 20: set([15,19,25]), 21: set([16,22]), 22: set([21,17,23]), 23: set([22,18,24]), 24: set([23,19,25]), 25: set([24,20])}

prob_half = [[0 for i in range(25)] for j in range(25)]
prob_equal = [[0 for i in range(25)] for j in range(25)]

for i in range(25):
    num = i + 1  # Werid stuff because of 0-indexed vs 1 indexed
    adj_squares = adj[num]
    num_adj = len(adj_squares)

    prob_equal[i][i] = 1/(1+num_adj)  #Chance to stay on same square
    prob_half[i][i] = 1/2             # ''                      ''
    
    for adj_square in adj_squares:
        j = adj_square - 1  # back to 0 indexed
        prob_equal[i][j] = 1/(1+num_adj)
        prob_half[i][j] = 1/(2*num_adj)

prob_half_copy = [row[:] for row in prob_half]
prob_equal_copy = [row[:] for row in prob_equal]

cur_half = numpy.array(prob_half_copy)
cur_equal = numpy.array(prob_equal_copy)
for i in range(201):
    if i % 20 == 0:
        print("HALF")
        print(cur_half)
        print()
        print("EQUAL")
        print(cur_equal)
        print()
        print()
    cur_half = numpy.dot(prob_half, cur_half)
    cur_equal = numpy.dot(prob_equal,cur_equal)


final_half = cur_half[0]
final_equal = cur_equal[0]

for l in (final_equal, final_half):
    for i in range(0,25,5):
        print(l[i:i+5])
    print()
