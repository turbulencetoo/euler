#!/usr/bin/env python

fpp = 1
fp = 1
i = 3

while 1:
    new = fp + fpp

    if len(str(new)) >= 1000:
        print(i, new)
        break

    fpp = fp
    fp = new
    i += 1
