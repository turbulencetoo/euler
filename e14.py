#!/usr/bin/env python3
MEMO = {1:0}

def collatz(num, steps):
    new_num = num/2 if (num%2==0) else 3*num + 1
    if new_num in MEMO:
        return steps+MEMO[new_num]
    else:
        return collatz(new_num, steps+1)

def main():
    for i in range(2, 1000000):
        MEMO[i] = collatz(i,0)
    print(max(MEMO.items(), key=lambda val: val[1]))

if __name__ == '__main__':
    main()


