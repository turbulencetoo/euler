#!/usr/bin/env python3
from random import random as rand
import sys

NUMTRIES = int(sys.argv[1])
START = float(sys.argv[2])
STEP = float(sys.argv[3])
NUMSTEPS = int(sys.argv[4])

def win20(q):
    wins = 0
    for x in range(1,51):
        prob = (1-(x/q))
        if rand() < prob:
            wins += 1
            if wins > 20:
                return False
    return wins == 20


q = START
for i in range(NUMSTEPS):
    wins = 0
    for i in range(NUMTRIES):
        if win20(q):
            wins += 1
    print(q, wins/NUMTRIES)
    q += STEP